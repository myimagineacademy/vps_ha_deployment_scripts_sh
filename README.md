# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repo is to store all the versio install shell scripts for a traditional versio on premise solution.
includes the following CentOS servers 
- HAProxy Servers
- PostgreSQL Servers
- RabbitMQ Servers

# Creation of VM servers
- locate ISO from install location
- Set Server Hostname and apply
- Set Server Network Address, Subnet, GW and DNS
- Set the install destination disk as the entire vm disk.
- Set the timezone and NTP details
- Set the root password - this should be jamesrivers80
- Create a new user - imagine password imagine
- Add to group wheel

## Install of local REPO server
if required you should install a local repo server. Please refer to the `Load Balancing and Redundancy Guide Lines' PDF. Install this CentOS based server and bootstrap as outlined in the guide. 

### Install steps for HA, PG and MQ Servers
1. Deploy the all CentOS Servers complete with hostname and IP address completed. This includes:
- HA 1,2,3
- PG 1,2,3
- MQ 1,2,3
1. Complete the NETWORK_VALUES form located in the `O.INPUT_FILES` directory. This will hold all the IP Address for the servers that you are going to deploy in the platform - including core services and motion etc...
1. There is an example `NETWORK_VALUES` file provided.
### HAProxy install steps
1. Upload the completed `NETWORK_VALUES' file to the home directory of the `imagine` user account.
1. Open the folder `2.HAPROXY_SVR` 
1. If you are using a local repo then please opt for the folder `a.bootstrap_local_repo_hapgmq_svr`
	1. Upload the .tar.gz to the home directory of the `imagine` user.
1. if you are installing the HA server via the online repos opt for the folder `b.bootstrap_online_hapgmq_svr`
	1. Upload the .tar.gz to the home directory of the `im2.HAPROXY_SVR`
1. Once uploaded unpack the .tar.gz
	1. `tar -xf bootstrap_online_hapgmq_svr.tar.gz`
	1. cd to the bootstrap dir 
	1. run the `bootstrap.sh'vim 


* Version

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
- [James Rivers](james.rivers@imaginecommunications.com)
