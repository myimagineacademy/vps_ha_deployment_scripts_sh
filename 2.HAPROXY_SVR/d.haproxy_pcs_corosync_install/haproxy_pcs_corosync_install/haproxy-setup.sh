#!/bin/bash

# SET INSTANCE VARS
USERNAME=$(whoami)
SCRIPTS_DIR=${HOME}/haproxy_pcs_corosync_install
HAPROXY_CFG_DIR=${HOME}/haproxy_cfg
HAPROXY_SVC_DIR=${HOME}/haproxy_svc

echo ""
## TESTS TO ENSURE WE CAN EVEN RUN THIS SCRIPT
if [[ ! ${USERNAME} = "root" ]]; then
    echo "Please Run as Root"
    exit 0
fi

echo ""
echo "       Setup HAproxy        "
echo ""
echo "this may take a few mins/hours"
echo ""

## Check if Bootstrap has been done
if [[ ! -f ${HOME}/.bootstrap_complete ]]; then
    echo "#########################################################"
    echo ""
    echo "Please run Bootstrap for this server..."
    echo "This server needs to be bootstrapped before installing HAProxy"
    echo "########################################################"
    exit 0
fi

# look for STANDARD_VARS
if [[ ! -f ${HOME}/NETWORK_VALUES ]]; then
    echo "########################################################"
    echo ""
    echo "  Ensure NETWORK_VALUES is in ${HOME}/ "
    echo " "
    echo "########################################################"
    exit 0
fi

## Already Run
if [[  -f ${HOME}/.haproxy_complete ]]; then
    echo "############################################"
    echo ""
    echo "  This  Script has already run "
    echo ""
    echo " if you wish to rerun it you need to"
    echo " "
    echo " rm ${HOME}/.haproxy_complete "
    echo ""
    echo "############################################"
    exit 0
fi
##############################################
echo "LOAD NETWORK_VALUES"
## Load Variables
source ${HOME}/NETWORK_VALUES
##############################################
echo ""
echo "Install HAproxy"
yum install -y haproxy policycoreutils-python

echo "SELinux Boolead"
setsebool -P haproxy_connect_any 1
##########################VIP####################
echo "Test HAproxy is Installed"
haproxy -v
##############################################
echo ""
echo "Firewall"
# Status Pages Ports
firewall-cmd --permanent --add-port=1972/tcp
firewall-cmd --permanent --add-port=1973/tcp
firewall-cmd --permanent --add-port=1975-1981/tcp
# Service Ports
firewall-cmd --permanent --add-port=80/tcp
firewall-cmd --permanent --add-port=81/tcp
firewall-cmd --permanent --add-port=5672/tcp
firewall-cmd --permanent --add-port=37036/tcp
firewall-cmd --permanent --add-port=9872/tcp
firewall-cmd --permanent --add-port=8899/tcp
firewall-cmd --permanent --add-port=5432/tcp
firewall-cmd --permanent --add-port=10442/tcp
firewall-cmd --permanent --add-port=51634/tcp
firewall-cmd --permanent --add-port=9005/tcp
firewall-cmd --permanent --add-port=1971/tcp
firewall-cmd --permanent --add-port=8080/tcp
firewall-cmd --permanent --add-port=1974/tcp
firewall-cmd --permanent --add-port=1970/tcp
firewall-cmd --reload

##############################################

echo "sed NETWORK_VALUES"

sed -i s/VIP/${VIP}/ ${HAPROXY_CFG_DIR}/*.cfg

sed -i s/PP1/${PP1}/ ${HAPROXY_CFG_DIR}/postgresql.cfg
sed -i s/PP2/${PP2}/ ${HAPROXY_CFG_DIR}/postgresql.cfg
sed -i s/PP3/${PP3}/ ${HAPROXY_CFG_DIR}/postgresql.cfg

sed -i s/MP1/${MP1}/ ${HAPROXY_CFG_DIR}/motion.cfg
sed -i s/MP2/${MP2}/ ${HAPROXY_CFG_DIR}/motion.cfg
sed -i s/MP3/${MP3}/ ${HAPROXY_CFG_DIR}/motion.cfg

sed -i s/SP1/${SP1}/ ${HAPROXY_CFG_DIR}/mastercontrol.cfg
sed -i s/SP2/${SP2}/ ${HAPROXY_CFG_DIR}/mastercontrol.cfg
sed -i s/SP3/${SP3}/ ${HAPROXY_CFG_DIR}/mastercontrol.cfg

sed -i s/SP1/${SP1}/ ${HAPROXY_CFG_DIR}/ingest.cfg
sed -i s/SP2/${SP2}/ ${HAPROXY_CFG_DIR}/ingest.cfg
sed -i s/SP3/${SP3}/ ${HAPROXY_CFG_DIR}/ingest.cfg

sed -i s/RP1/${RP1}/ ${HAPROXY_CFG_DIR}/rabbitmq.cfg
sed -i s/RP2/${RP2}/ ${HAPROXY_CFG_DIR}/rabbitmq.cfg
sed -i s/RP3/${RP3}/ ${HAPROXY_CFG_DIR}/rabbitmq.cfg

sed -i s/SP1/${SP1}/ ${HAPROXY_CFG_DIR}/{websocket.cfg,ui.cfg,httpbridge.cfg,hermes.cfg,heartbeat.cfg,content.cfg}
sed -i s/SP2/${SP2}/ ${HAPROXY_CFG_DIR}/{websocket.cfg,ui.cfg,httpbridge.cfg,hermes.cfg,heartbeat.cfg,content.cfg}
sed -i s/SP3/${SP3}/ ${HAPROXY_CFG_DIR}/{websocket.cfg,ui.cfg,httpbridge.cfg,hermes.cfg,heartbeat.cfg,content.cfg}

##############################################

echo "Set nf_conntrack Hash Size"
cat > /etc/modprobe.d/nf_conntrack.conf <<EOF
options  nf_conntrack hashsize=49152

EOF
##############################################
KERN_MOD=$(cat /etc/sysctl.d/99-sysctl.conf | awk /ip_nonlocal_bind/)
if [[ ! ${KERN_MOD} =~ "ip_nonlocal_bind" ]]; then
    echo "Kernel Mods"
    cat >> /etc/sysctl.d/99-sysctl.conf <<EOF

net.netfilter.nf_conntrack_max=196608
net.ipv4.tcp_tw_reuse=1
fs.file-max=2097152
net.ipv4.ip_local_port_range =  37040    65535
net.ipv4.ip_nonlocal_bind=1

EOF

echo "Load into Kernel"
    sysctl -p

fi
##############################################
mv -f ${HAPROXY_CFG_DIR}/*.cfg /etc/haproxy
mv -f ${HAPROXY_SVC_DIR}/*.service /etc/systemd/system/
##############################################
## fix SELinux ownership permissions
chcon -R -t etc_t /etc/haproxy/
##############################################
systemctl daemon-reload

echo "Start Services"
systemctl start content.service
systemctl start heartbeat.service
systemctl start hermes.service
systemctl start httpbridge.service
systemctl start postgresql.service
systemctl start rabbitmq.service
systemctl start ui.service
systemctl start websocket.service
systemctl start motion.service
systemctl start mastercontrol.service
systemctl start ingest.service
##############################################
echo ""
echo "Service Status"
echo "############################"
echo "Content"
systemctl status content.service
echo ""
echo "############################"
echo "Heartbeat"
systemctl status heartbeat.service
echo ""
echo "############################"
echo "Hermes"
systemctl status hermes.service
echo ""
echo "############################"
echo "Httpbridge"
systemctl status httpbridge.service
echo ""
echo "############################"
echo "Ingest"
systemctl status ingest.service
echo ""
echo "############################"
echo "Mastercontrol"
systemctl status mastercontrol.service
echo ""
echo "############################"
echo "Motion"
systemctl status motion.service
echo ""
echo "############################"
echo "PostgreSQL"
systemctl status postgresql.service
echo ""
echo "############################"
echo "RabbitMQ"
systemctl status rabbitmq.service
echo ""
echo "############################"
echo "UI"
systemctl status ui.service
echo ""
echo "############################"
echo "Websocket"
systemctl status websocket.service
echo ""
echo "############################"
echo ""
##############################################
echo "Stop Services"

systemctl stop content.service
systemctl stop heartbeat.service
systemctl stop hermes.service
systemctl stop httpbridge.service
systemctl stop mastercontrol.service
systemctl stop postgresql.service
systemctl stop rabbitmq.service
systemctl stop ui.service
systemctl stop websocket.service
systemctl stop mastercontrol.service
systemctl stop motion.service
systemctl stop ingest.service

##############################################
    echo "Install Pacemaker/Corosync with PCS"
    yum install -y pcs
##############################################
    echo ""
    echo "Firewall"
    firewall-cmd --permanent --add-port=2224/tcp
    firewall-cmd --permanent --add-port=5404-5406/udp
    firewall-cmd --reload
##############################################
  echo -e "hacluster\nhacluster" | passwd hacluster
##############################################
    echo "Enable Service"
    systemctl enable pcsd.service
    systemctl enable corosync.service
    systemctl enable pacemaker.service
    echo""
    echo "Start PCS Services"
    systemctl start pcsd.service
    systemctl status pcsd.service
##############################################
echo "Clean up"
mkdir ${HOME}/haproxy_pcs_corosync_install_complete
mv ${HOME}/haproxy_pcs_corosync_install ${HOME}/haproxy_pcs_corosync_install_complete
mv ${HOME}/haproxy_pcs_corosync_install.tar.gz ${HOME}/haproxy_pcs_corosync_install_complete
mv ${HOME}/haproxy_cfg ${HOME}/haproxy_pcs_corosync_install_complete
mv ${HOME}/haproxy_svc ${HOME}/haproxy_pcs_corosync_install_complete
touch ${HOME}/.haproxy_complete


exit $?
