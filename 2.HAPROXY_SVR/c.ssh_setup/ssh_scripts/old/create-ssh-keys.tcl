#!/usr/bin/env expect


spawn ssh-keygen

expect "id_rsa):"
send "\n"

expect {
	"(y/n)?"
	{
		send "n\r"
		puts "already there"
		exit
	 }
	"passphrase):"
	{
		send "\n"
		puts "enter"
	}
}
expect "again:"
send "\n"

puts "enter"

expect eof


