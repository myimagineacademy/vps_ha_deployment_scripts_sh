#!/usr/bin/expect

set host_list "~/.local-nodes"
set PASSWORD "LINUX_PASSWORD"

set f [open $host_list]
set hosts [split [read $f] "\n" ]
close $f

foreach addr $hosts {
    puts $addr
    if { $addr eq "" } {
        puts "empty host"

    } else {
        spawn ssh-copy-id -f $addr
        expect {
                "(yes/no)?"
            {
                send "yes\r"
            }
                "password: "
            {
                send "$PASSWORD\r"
            }
            "added."
        {
            puts "already added"
        }
        }
        expect {
                "password: "
            {
                send "$PASSWORD\r"
            }
                "added."
            {
                puts "already added"
            }
            }
    }
}


exit 0

