
SSH_SCRIPTS_DIR="${HOME}/ssh_scripts"
RERUN_SSH_SCRIPTS="${HOME}/ssh_complete/ssh_scripts"
## Check if Bootstrap has been done
if [[ ! -f ${HOME}/.bootstrap_complete ]]; then
    echo "#########################################################"
    echo ""
    echo "Please run Bootstrap for this server..."
    echo "This server needs to be bootstrapped before configuring SSH"
    echo "########################################################"
    exit 0
fi

# look for STANDARD_VARS
if [[ ! -f ${HOME}/NETWORK_VALUES ]]; then
    echo "########################################################"
    echo ""
    echo "  Ensure NETWORK_VALUES is in ${HOME}/ "
    echo " "
    echo "########################################################"
    exit 0
fi
##############################################
echo "LOAD NETWORK VALUES"
## Load Variables
source ${HOME}/NETWORK_VALUES
##############################################
mkdir -m 700 ${HOME}/.ssh/
##############################################
## SSH
chmod +x ${RERUN_SSH_SCRIPTS}/{create-ssh-keys.tcl,copy-ssh-id.tcl}
echo ""
echo "Create Keys"
${RERUN_SSH_SCRIPTS}/create-ssh-keys.tcl
echo ""
##############################################
# [TODO] make as a loop of sorts to deal with more then 3 of each

echo "Populate SSH Config"
rm -rf ${HOME}/.ssh/config

cat >> ${HOME}/.ssh/config <<EOF

## HAproxy- Services
Host ha1-svc
        Hostname ${SVC_HP1}
        User root
Host ha2-svc
        Hostname ${SVC_HP2}
        User root
Host ha3-svc
        Hostname ${SVC_HP3}
        User root

###NOTUSED
## HAproxy -PG Database
#Host ha1-db
#        Hostname ${DB_HP1}
#        User root
#Host ha2-db
#        Hostname ${DB_HP2}
#        User root
#Host ha3-db
#        Hostname ${DB_HP3}
#        User root

## PostgreSQL
Host pg1
        Hostname ${PP1}
        User root
Host pg2
        Hostname ${PP2}
        User root
Host pg3
        Hostname ${PP3}
        User root
EOF
##############################################
# Local Nodes
rm -rf ${HOME}/.local-nodes
cat >> ${HOME}/.local-nodes << EOF
ha1-svc
ha2-svc
ha3-svc
pg1
pg2
pg3
ha1-db
ha2-db
ha3-db

EOF
##############################################
echo "Populate Hosts File"
rm -rf /etc/hosts

cp ${RERUN_SSH_SCRIPTS}/hosts /etc/hosts

cat >> /etc/hosts << EOF
# PostgeSQL Nodes
${PP1}  ${PH1}${DOMAIN} ${PH1}
${PP2}  ${PH2}${DOMAIN} ${PH2}
${PP3}  ${PH3}${DOMAIN} ${PH3}

# HAproxy Nodes Service
${SVC_HP1}  ${SVC_HH1}${DOMAIN} ${SVC_HH1}
${SVC_HP2}  ${SVC_HH2}${DOMAIN} ${SVC_HH2}
${SVC_HP3}  ${SVC_HH3}${DOMAIN} ${SVC_HH3}

##NOT CURRENTLY USED
# HAproxy Nodes DB
#${DB_HP1} ${DB_HH1}${DOMAIN}  ${DB_HH1}
#${DB_HP2} ${DB_HH2}${DOMAIN}  ${DB_HH2}
#${DB_HP3} ${DB_HH3}${DOMAIN}  ${DB_HH3}
EOF
##############################################

## clean out .none domain from hosts file
sed -i s/[a-zA-Z0-9]*\.none// /etc/hosts

echo "Copy SSH Keys to other Nodes"
source ${HOME}/NETWORK_VALUES

sed -i s/LINUX_PASSWORD/${LINUX_PASSWORD}/g ${RERUN_SSH_SCRIPTS}/copy-ssh-id.tcl

${RERUN_SSH_SCRIPTS}/copy-ssh-id.tcl

##############################################
echo "Clean up"
touch ${HOME}/.rerun_ssh_complete


exit $?
