PACEMAKER_SCRIPTS_DIR="${HOME}/pacemaker_scripts"


##############################################
    echo ""
    echo "       Pacemaker Configuration       "
    echo ""
    echo "      this may take a few mins"
    echo ""
##############################################
## Check if Bootstrap has been done
if [[ ! -f ${HOME}/.bootstrap_complete ]]; then
    echo "#########################################################"
    echo ""
    echo "Please run Bootstrap for this server..."
    echo "This server needs to be bootstrapped before installing HAProxy"
    echo "########################################################"
    exit 0
fi
##############################################
# look for STANDARD_VARS
if [[ ! -f ${HOME}/NETWORK_VALUES ]]; then
    echo "########################################################"
    echo ""
    echo "  Ensure NETWORK_VALUES is in ${HOME}/ "
    echo " "
    echo "########################################################"
    exit 0
fi
##############################################
## Already Run
if [[ ! -f ${HOME}/.haproxy_complete ]]; then
    echo "############################################"
    echo ""
    echo " PCS Install has not been done "
    echo ""
    echo "############################################"
    exit 0
fi
##############################################
## Already Run
if [[  ! -f ${HOME}/.ssh_complete ]]; then
    echo "############################################"
    echo ""
    echo "  SSH setup has not been run "
    echo ""
    echo "############################################"
    exit 0
fi
##############################################
chown -R root:root /root/
##############################################
echo "LOAD NETWORK_VALUES"
## Load Variables
source ${HOME}/NETWORK_VALUES
##############################################
    echo "Substitute VARIABLES with DOMAIN  ==>  ${DOMAIN}"
    sed -i s/HAPROXY1/"${SVC_HH1}${DOMAIN}"/ ${PACEMAKER_SCRIPTS_DIR}/*.sh
    sed -i s/HAPROXY2/"${SVC_HH2}${DOMAIN}"/ ${PACEMAKER_SCRIPTS_DIR}/*.sh
    sed -i s/HAPROXY3/"${SVC_HH3}${DOMAIN}"/ ${PACEMAKER_SCRIPTS_DIR}/*.sh
##############################################
    systemctl start pcsd.service
##############################################
    echo "Authenticate Cluster Nodes"
    ${PACEMAKER_SCRIPTS_DIR}/authenticate.sh
##############################################
    echo "Create Cluster"
    ${PACEMAKER_SCRIPTS_DIR}/create-cluster.sh
##############################################
    echo "RE-enable Service"
    systemctl enable pcsd.service
    systemctl enable corosync.service
    systemctl enable pacemaker.service
    echo""
##############################################
    echo "Ensure All Services are Still  "
    systemctl start pcsd.service
    systemctl start corosync.service
    systemctl start pacemaker.service
##############################################
    echo "Ensure All Services are Still  "
    systemctl status pcsd.service
    systemctl status  corosync.service
    systemctl status  pacemaker.service
##############################################
    sleep 10
##############################################
    source ${HOME}/NETWORK_VALUES
    ssh ${SVC_HP2}  "systemctl enable pcsd.service;systemctl enable corosync.service;systemctl enable pacemaker.service; systemctl start pcsd.service;systemctl start corosync.service;systemctl start pacemaker.service;"
    ssh ${SVC_HP3}  "systemctl enable pcsd.service;systemctl enable corosync.service;systemctl enable pacemaker.service; systemctl start pcsd.service;systemctl start corosync.service;systemctl start pacemaker.service;"
##############################################
    echo "Check Cluster Status"
if [[ $(/usr/sbin/pcs status) =~ ".*Error.*" ]] ; then
    echo "PCS Through an Error Please Investigate"
    systemctl status pcsd.service
    systemctl status corosync.service
    systemctl status pacemaker.service
    exit 1
fi
##############################################
    echo ""
    echo "Set up Resources"
    echo ""
    echo "Set up the Virtual IP"
    /usr/sbin/pcs resource create ClusterIP ocf:heartbeat:IPaddr2 ip=$VIP  cidr_netmask=$MASK arp_bg=true arp_count=10 arp_interval=200
##############################################
    echo "Set up the Rest of the Resources"
    ${PACEMAKER_SCRIPTS_DIR}/resource_services.sh
##############################################
    echo "Add aliases"
    echo "alias cluster-state='/usr/sbin/pcs cluster status'" >> ${HOME}/.bashrc
    echo "alias cluster-status='/usr/sbin/pcs status'" >> ${HOME}/.bashrc
##############################################
    pcs status
##############################################

    echo "Clean up"
    mkdir ${HOME}/pcs_configure_complete
    touch ${HOME}/.pcs_complete
    mv ${HOME}/pacemaker_scripts ${HOME}/pcs_configure_complete/
    mv ${HOME}/configure_pacemaker_HA1_ONLY.sh ${HOME}/pcs_configure_complete/
