#!/bin/bash


/usr/sbin/pcs cluster setup \
            HAPROXY1 \
            HAPROXY2 \
            HAPROXY3 \
            -u hacluster -p hacluster \
            --name ImagineVersioCluster \
            --force


systemctl start pcsd.service
systemctl start corosync.service
systemctl start pacemaker.service
systemctl enable corosync.service
systemctl enable pacemaker.service



# to allow down to one node functioning set these policies
/usr/sbin/pcs property set  no-quorum-policy=ignore
/usr/sbin/pcs property set  stonith-enabled=false
