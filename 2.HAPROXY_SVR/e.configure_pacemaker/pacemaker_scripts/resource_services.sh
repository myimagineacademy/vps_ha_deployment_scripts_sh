#!/bin/bash
# Content
/usr/sbin/pcs resource create content systemd:content.service
/usr/sbin/pcs constraint colocation add content with ClusterIP INFINITY
sleep 3

# RabbitMQ
/usr/sbin/pcs resource create rabbitmq systemd:rabbitmq.service
/usr/sbin/pcs constraint colocation add rabbitmq with ClusterIP INFINITY
sleep 3

# Websocket
/usr/sbin/pcs resource create websocket systemd:websocket.service
/usr/sbin/pcs constraint colocation add websocket with ClusterIP INFINITY
sleep 3

# Hermes
/usr/sbin/pcs resource create hermes systemd:hermes.service
/usr/sbin/pcs constraint colocation add hermes with ClusterIP INFINITY
sleep 3

# Heartbeat
/usr/sbin/pcs resource create heartbeat systemd:heartbeat.service
/usr/sbin/pcs constraint colocation add heartbeat with ClusterIP INFINITY
sleep 3

# UI
/usr/sbin/pcs resource create ui systemd:ui.service
/usr/sbin/pcs constraint colocation add ui with ClusterIP INFINITY
sleep 3

# HTTPbridge
/usr/sbin/pcs resource create httpbridge systemd:httpbridge.service
/usr/sbin/pcs constraint colocation add httpbridge with ClusterIP INFINITY
sleep 3

# PostgreSQL
/usr/sbin/pcs resource create postgresql systemd:postgresql.service
/usr/sbin/pcs constraint colocation add postgresql with ClusterIP INFINITY
sleep 3

# MasterControl
/usr/sbin/pcs resource create mastercontrol systemd:mastercontrol.service
/usr/sbin/pcs constraint colocation add mastercontrol with ClusterIP INFINITY
sleep 3

# Motion
/usr/sbin/pcs resource create motion systemd:motion.service
/usr/sbin/pcs constraint colocation add motion with ClusterIP INFINITY
sleep 3

# Ingest
/usr/sbin/pcs resource create ingest systemd:ingest.service
/usr/sbin/pcs constraint colocation add ingest with ClusterIP INFINITY
sleep 3



sleep 10

/usr/sbin/pcs cluster status


exit $?
