#!/bin/bash


LOCAL_REPOS="base centosplus extras updates epel pgdg96 erlang-solutions bintray-rabbitmq-server"


echo "STOPING NGINX FOR UPDATE"
systemctl stop nginx

echo "pip repo"
pip2tgz /db/repos/pip/ -r /db/.requirements.txt
dir2pi -n /db/repos/pip/
pip2pi  /db/repos/pip/ -n -r /db/.requirements.txt

echo "yum repos"
for REPO in ${LOCAL_REPOS}; do
	reposync -g -l -d -m --repoid=$REPO --newest-only --download-metadata --download_path=/db/repos/
	createrepo --update --workers=6 /db/repos/$REPO/
	chcon -R -t httpd_sys_content_t /db/repos/
	echo ""
	echo "**************************************************"
	echo ""
	echo ""
	echo "           ${REPO} has been updated"
	echo ""
	echo ""
	echo "**************************************************"
done


systemctl start nginx

echo "STARTED NGINX -- UPDATE COMPLETE"



exit $?
