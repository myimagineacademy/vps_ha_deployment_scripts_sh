#!/bin/bash


##############################################
SCRIPTS_DIR="${HOME}/localrepoinstall"
REPO_DIR="/db/repos"
LOCAL_REPOS="base centosplus extras updates epel pgdg96 erlang-solutions bintray-rabbitmq-server"
NGINX_DIR='/etc/nginx/conf.d'
USERNAME=$(whoami)
HOST_IPS=$(hostname -I)
MY_HOSTNAME=$(hostname | awk -F . '{print $1}')
##############################################
if [[ ${USERNAME} != "root" ]]; then
    echo "Please Run as Root"
    exit 0
fi

## Check if Bootstrap has been done
if [[ ! -f ${HOME}/.bootstrap_complete ]]; then
    echo "#########################################################"
    echo ""
    echo "Please run Bootstrap for this server..."
    echo "This server needs to be bootstrapped before installing HAProxy"
    echo "########################################################"
    exit 0
fi
##############################################



## Already Run
if [[  -f ${HOME}/repo-setup/.repo_complete ]]; then
    echo "############################################"
    echo ""
    echo "  This  Script has already run "
    echo ""
    echo " if you wish to rerun it you need to"
    echo " "
    echo " rm ${HOME}/repo-setup/.repo_complete "
    echo ""
    echo "############################################"
    exit 0
fi


##############################################
echo ""
echo "Install_Local_Repo Version 4.3"
echo ""
echo "this may take a few mins or hours "
echo ""
##############################################
echo ""
echo ""
echo "Enable TCP/80"
firewall-cmd --permanent --add-port=80/tcp
firewall-cmd --reload
##############################################
## Get Source Repos with Keys
yum -y install https://packages.erlang-solutions.com/erlang-solutions-1.0-1.noarch.rpm
rpm --import https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc
rpm --import https://packages.erlang-solutions.com/rpm/erlang_solutions.asc
yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
##############################################
##############################################
echo ""
echo "Adjust Repo Files"
rm /etc/yum.repos.d/*.repo
mv -f ${SCRIPTS_DIR}/*.repo /etc/yum.repos.d/
rpm --import /etc/pki/rpm-gpg/*
##############################################
echo "Get required packages"
yum install -y createrepo nginx
##NGINX_DIR='/etc/nginx/conf.d'
##############################################
pip3 install pip2pi
##############################################
echo "Prepare Repo Directory"
mkdir -p ${REPO_DIR}/pip
##############################################
echo ""
echo "Create Local pip Repo"
echo ""
mv ${SCRIPTS_DIR}/requirements.txt /db/.requirements.txt
##############################################
##############################################
echo ""
echo "**************************************************"
echo "             Pull the Repos Down"
echo ""
echo "This will take a long time... go get a coffee (~hour+)"
echo ""
echo ""
echo "**************************************************"
echo ""
echo "Create Local pip Repo"
echo ""
##############################################
##############################################
pip2tgz ${REPO_DIR}/pip/ -r /db/.requirements.txt
dir2pi -n ${REPO_DIR}/pip/
pip2pi  ${REPO_DIR}/pip/ -n -r /db/.requirements.txt
##############################################
##############################################
## loop to update repos one at a time
for REPO in ${LOCAL_REPOS}; do
        echo "**************************************************"
        echo ""
        echo ""
        echo "           ${REPO} is Starting"
        echo ""
        echo ""
        echo "**************************************************"
        reposync -g -l -d -m --repoid=${REPO} --newest-only --download-metadata --download_path=${REPO_DIR}/
        createrepo --update --workers=6 ${REPO_DIR}/${REPO}/
        echo "**************************************************"
        echo ""
        echo ""
        echo "           ${REPO} is Finished"
        echo ""
        echo ""
        echo "**************************************************"
        echo ""
done
##############################################
##############################################
echo "Fix Permissions"
echo ""
setsebool -P httpd_can_network_connect 1
chmod 775 -R ${REPO_DIR}
chown -R root:nginx ${REPO_DIR}
chcon -R -t httpd_sys_content_t ${REPO_DIR}/
##############################################
##############################################
echo "Install Cron Script"
echo ""
mv -f ${SCRIPTS_DIR}/update-repos.sh /etc/cron.weekly/update-repos.sh
chmod +x /etc/cron.weekly/update-repos.sh
##############################################
##############################################
echo "Configure Nginx"
echo ""

if [[ ! -d ${NGINX_DIR} ]]; then
    echo "Add missing directory"
    mkdir ${NGINX_DIR}
fi
mv -f ${SCRIPTS_DIR}/nginx.conf /etc/nginx/nginx.conf
mv -f ${SCRIPTS_DIR}/repo.conf  ${NGINX_DIR}/repo.conf
##############################################
##############################################
echo "SElinux Permissions"
chcon -R -t httpd_config_t /etc/nginx
##############################################
##############################################
echo ""
echo "Enable Nginx"
systemctl enable nginx.service
echo "Start Nginx"
systemctl stop nginx.service
systemctl start nginx.service
echo ""
echo "**************************************************"
echo ""
systemctl status -l nginx.service
echo ""
echo "**************************************************"
##############################################
##############################################
echo "Add Alias"
echo "up'" >> ${HOME}/.bashrc
##############################################
##############################################
echo "Cleanup"

mkdir ${HOME}/repo_complete
mv  ${HOME}/localrepoinstall ${HOME}/repo_complete/
mv  ${HOME}/localrepoinstall.tar.gz ${HOME}/repo_complete/
touch ${HOME}/.repo_complete
