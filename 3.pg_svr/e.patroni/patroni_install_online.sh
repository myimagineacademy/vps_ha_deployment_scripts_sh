#!/bin/bash

# SET INSTANCE VARS
USERNAME=$(whoami)
PATRONI_SCRIPTS_DIR="${HOME}/patroni-scripts"
REPLICATION_PW="Imagine01"
POSTGRES_PW="Imagine01"
HERMES_PW="Imagine01"
PG_VERSION="96"
PG_VERSION_DOT="9.6"
PIP_VERSION="1.6.0"
MY_HOSTNAME=$(hostname)



echo ""
## TESTS TO ENSURE WE CAN EVEN RUN THIS SCRIPT

if [[ ! ${USERNAME} = "root" ]]; then
    echo "Please Run as Root"
    exit 0
fi

if [[ ! -f  ${HOME}/.bootstrap_complete ]]; then
    echo "############################################"
    echo ""
    echo "Please run Bootstrap and Setup STANDARD_VARS"
    echo ""
    echo " Please See the README in bootstrap/ "
    echo " found in the root directory of the zip"
    echo " you downloaded"
    echo ""
    echo "############################################"
    exit 0
fi
if [[ ! -f ${HOME}/.consul_complete ]]; then
    echo "############################################"
    echo ""
    echo " Consul Must be Installed First "
    echo ""
    echo "############################################"
    exit 0
fi
if [[ -f ${HOME}/.patroni_complete ]]; then
    echo "############################################"
    echo ""
    echo " Patroni has already been Installed  "
    echo ""
    echo " if you wish to force it to rerun "
    echo "          rm ${HOME}/.patroni_complete "
    echo " and try again "
    echo ""
    echo "############################################"
    exit 0
fi
##############################################
echo "LOAD NETWORK VALUES"
source ${HOME}/NETWORK_VALUES
##############################################
# make sure everything has the right ownership/group
chown -R ${USERNAME}:${USERNAME} ${PATRONI_SCRIPTS_DIR}/
##############################################
yum install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
## yum update
## yum -y install postgresql96-contrib postgresql96-server postgresql96-devel
echo "Install PostgreSQL ${PG_VERSION_DOT}"
echo ""
yum -y install postgresql${PG_VERSION}-contrib postgresql${PG_VERSION}-server postgresql${PG_VERSION}-devel
echo ""
##############################################
echo "Patroni and Dependencies"
pip3 install --upgrade setuptools urllib3==1.24.2
pip3 install patroni==${PIP_VERSION} python-consul psycopg2-binary
##############################################
echo ""
echo "Continue with the Installation"
echo ""
mkdir -p /db/patroni
mkdir -p /db/pg/{log,run,backup}
##############################################
echo "Setup Patroni as a Service"
mv -f ${PATRONI_SCRIPTS_DIR}/patroni.service /etc/systemd/system/patroni.service

echo "Populate Patroni Config Yaml"
sed -i 's/MY_IP/'"${MY_IP}"'/' ${PATRONI_SCRIPTS_DIR}/patroni-config.yml
sed -i 's/HERMES_PW/'"${HERMES_PW}"'/'  ${PATRONI_SCRIPTS_DIR}/patroni-config.yml
sed -i 's/REPLICATION_PW/'"${REPLICATION_PW}"'/' ${PATRONI_SCRIPTS_DIR}/patroni-config.yml
sed -i 's/POSTGRES_PW/'"${POSTGRES_PW}"'/' ${PATRONI_SCRIPTS_DIR}/patroni-config.yml
sed -i 's/MY_HOSTNAME/'"${MY_HOSTNAME}"'/' ${PATRONI_SCRIPTS_DIR}/patroni-config.yml


mv -f ${PATRONI_SCRIPTS_DIR}/patroni-config.yml /db/patroni/patroni-config.yml
chown -R postgres:postgres /db/pg   /db/patroni

echo ""
echo "Firewall"
firewall-cmd --permanent --add-port=8008/tcp
firewall-cmd --permanent --add-port=8008/udp
firewall-cmd --permanent --add-port=5432/tcp
firewall-cmd --reload


echo ""
echo "Enable and Start the Service"
echo ""
systemctl daemon-reload
systemctl enable patroni.service
systemctl stop patroni.service
systemctl start patroni.service
sleep 5

echo ""
echo "Service Status"
echo "############################"
echo ""
systemctl status patroni.service
echo ""
echo "############################"
echo ""
sleep 5

echo "Add to .bashrc File Paths and Aliases"
echo "alias whosmaster='/usr/local/bin/patronictl -c /db/patroni/patroni-config.yml list'" >> ${HOME}/.bashrc
echo "alias watch-whosmaster='watch -n1 --differences --color --exec /usr/local/bin/patronictl -c /db/patroni/patroni-config.yml list'" >> ${HOME}/.bashrc
echo "export PATH=$PATH:/usr/pgsql-9.6/bin"  >> ${HOME}/.bashrc

source ${HOME}/.bashrc
echo "whosmater"
/usr/local/bin/patronictl -c /db/patroni/patroni-config.yml list


echo "Clean up"
mkdir ${HOME}/patroni_setup_complete/
mv ${HOME}/patroni_install.sh  ${HOME}/patroni-scripts ${HOME}/patroni_install.tar.gz ${HOME}/patroni_setup_complete/
touch ${HOME}/.patroni_complete


exit $?
