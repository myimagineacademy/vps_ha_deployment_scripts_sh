#!/usr/bin/env expect


spawn ssh-keygen

expect "id_rsa):"
send "\n"
puts "enter"

expect {
	"(y/n)?"
	{
		send "n\r"
		puts "already exists"
		exit
	 }
	"passphrase):"
	{
		send "\n"
		puts "enter"
	}
}
expect "again:"
send "\n"

puts "enter"

expect eof
