#!/bin/bash

CONSUL_SCRIPTS_DIR="${HOME}/consul_scripts"
USERNAME=$(whoami)
CONSULKEYNEW="${HOME}/.consul_key"

## Check if Bootstrap has been done
if [[ ! -f ${HOME}/.bootstrap_complete ]]; then
    echo "#########################################################"
    echo ""
    echo "Please run Bootstrap for this server..."
    echo "This server needs to be bootstrapped before configuring SSH"
    echo "########################################################"
    exit 0
fi
# look for Network Values
if [[ ! -f ${HOME}/NETWORK_VALUES ]]; then
    echo "########################################################"
    echo ""
    echo "  Ensure NETWORK_VALUES is in ${HOME}/ "
    echo " "
    echo "########################################################"
    exit 0
fi
#check for ssh
if [[ ! -f ${HOME}/.ssh_complete ]]; then
    echo "########################################################"
    echo ""
    echo "  Ensure that SSH configure has been run "
    echo " "
    echo "########################################################"
    exit 0
fi

if [[ ! -f ${HOME}/.rerun_ssh_complete ]]; then
    echo "########################################################"
    echo ""
    echo "  Ensure that SSH re run has been completed "
    echo " "
    echo "  If you are happy that the ssh-copy-id process has been done correctly "
    echo "  and all servers can be reached from this server via SSH with no prompts for passwords  "
    echo "  then run the command - touch ~/.rerun_ssh_complete  "
    echo " "
    echo "########################################################"
    exit 0
fi


##############################################
## Substitute in Array of IPs
echo  "Your LOCAL IP"
LOCALIP=$(hostname -I)
echo ${LOCALIP}
sed -i 's/LOCALIP/'"${LOCALIP}"'/' ~/NETWORK_VALUES
sed -i 's/ "/"/' ~/NETWORK_VALUES

##############################################
echo "LOAD NETWORK_VALUES"
## Load Variables
source ${HOME}/NETWORK_VALUES
###################################he tar archive is truncated###########
##Set the array IP
ARRAY_IPS="${PP1} ${PP2} ${PP3} ${SVC_HP1} ${SVC_HP2} ${SVC_HP3}"
SSH_ARRAY="ha1-svc ha2-svc ha3-svc pg1 pg2 pg3"
###add at a later date -
##ARRAY_IPS="${PP1} ${PP2} ${PP3} ${SVC_HP1} ${SVC_HP2} ${SVC_HP3} ${DB_HP1} ${DB_HP2} ${DB_HP3}"
##SSH_ARRAY="ha1-svc ha2-svc ha3-svc pg1 pg2 pg3 ha1-db ha2-db ha3-db"
##############################################
# make sure everything has the right ownership/group
chown -R ${USERNAME}:${USERNAME} ${CONSUL_SCRIPTS_DIR}/
##############################################
echo "Install Consul"
unzip ${CONSUL_SCRIPTS_DIR}/consul_1.6.1_linux_amd64.zip

mv ${HOME}/consul /usr/local/bin/
echo "test its installed and in the path"
echo "Version should match 1.6.1"
consul -v
###################################he tar archive is truncated###########
# Consul Configure
mkdir -p /db/consul/{server,data,bootstrap}
useradd consul
##############################################
for IP in ${ARRAY_IPS}; do
    if [[ ${IP} != ${MY_IP} ]]; then
        Q_IP="\"${IP}\""
        if [[ $LIST_OF_IPS = "" ]]; then
            LIST_OF_IPS=${Q_IP}
        else
            LIST_OF_IPS="${LIST_OF_IPS},${Q_IP}"
        fi
    fi
done
##############################################
## If PG1 this is the initial node
if  [[ "${MY_IP}" = "${PP1}" ]] && [[ ${CONSULKEY} = "EMPTY" ]] ; then
    echo "Create Consul Key"
    /usr/local/bin/consul keygen  > ${CONSULKEYNEW}
    CONSUL_KEYGEN_KEY_NEW=$(cat ${CONSULKEYNEW})
fi
    for SSH_NAME in ${SSH_ARRAY}; do
    ssh ${SSH_NAME} sed -i 's/EMPTY/'"${CONSUL_KEYGEN_KEY_NEW}"'/' /root/NETWORK_VALUES
done
##############################################
if [[ "${MY_IP}" = "${PP1}" ]]; then
    INITIAL_NODE="true"
fi
##############################################
echo "LOAD NETWORK_VALUES"
## Load Variables
source ${HOME}/NETWORK_VALUES
##############################################
## Substitute in Array of IPs
echo "Substitute Values"
sed -i 's/MY_IP/'"${MY_IP}"'/'  ${CONSUL_SCRIPTS_DIR}/*
sed -i 's/LIST_OF_IPS/'"${LIST_OF_IPS}"'/'  ${CONSUL_SCRIPTS_DIR}/*.json
sed -i 's/CONSUL_KEYGEN_KEY/'"${CONSULKEY}"'/' ${CONSUL_SCRIPTS_DIR}/*.json
##############################################
echo ""
echo "Populate the Folders"
mv -f ${CONSUL_SCRIPTS_DIR}/bootstrap.json    /db/consul/bootstrap/
mv -f ${CONSUL_SCRIPTS_DIR}/server.json       /db/consul/server/
mv -f ${CONSUL_SCRIPTS_DIR}/consul.service    /etc/systemd/system/
mv -f ${CONSUL_SCRIPTS_DIR}/consul-bootstrap.service /etc/systemd/system/
##############################################
chown -R consul: /db/consul
##############################################
echo ""
echo "Firewall"
firewall-cmd --permanent --add-port=8300-8310/tcp
firewall-cmd --permanent --add-port=8500/tcp
firewall-cmd --permanent --add-port=8600/udp
firewall-cmd --reload
##############################################
echo "Add aliases"
echo "alias consul-keys='consul kv get --keys'" >> ${HOME}/.bashrc
echo "alias consul-leader='consul operator raft list-peers'" >> ${HOME}/.bashrc

##############################################
if [[ ${INITIAL_NODE} = "true" ]]; then
    echo ""
    echo "Enable and Start the Service"
    echo ""
    systemctl daemon-reload
    systemctl enable consul.service
    echo " Start the Consul Bootstrap Service"
    systemctl stop consul-bootstrap.service
    systemctl stop consul.service
    systemctl start consul-bootstrap.service
    sleep 5
    echo ""
    echo "Service Status"
    echo "############################"
    echo ""
    systemctl status -l consul-bootstrap.service
    echo ""
    echo "############################"
    echo ""
    sleep 7
    consul members
    sleep 7
    consul operator raft list-peers
    Sleep 7
    echo "########################################################"
    echo ""
    echo "    Consul has been Installed and Configured on this INITIAL NODE"
    echo " "
    echo "    Once all other nodes are complete, return to the initial node (pg1) and run"
    echo " "
    echo "    systemctl stop consul-bootstrap.service"
    echo " "
    echo "    systemctl start consul.service"
    echo " "
    echo "    systemctl status consul.service"
    echo " "
    echo "    consul members      "
    echo " "
    echo "########################################################"
  else
    echo ""
    echo "Enable and Start the Service"
    echo ""
    systemctl daemon-reload
    systemctl enable consul.service
    systemctl stop consul-bootstrap.service
    systemctl stop consul.service
    systemctl start consul.service
    sleep 5
    echo ""
    echo "Service Status"
    echo "############################"
    echo ""
    systemctl status -l consul.service
    echo ""
    echo "############################"
    echo ""
    sleep 3
    consul members
    sleep 5
    consul operator raft list-peers
    sleep 20
fi

echo "Clean up"

mkdir ${HOME}/consul_setup_conplete/
mv ${HOME}/consul_install.sh ${HOME}/consul_scripts ${HOME}/consul_install.tar ${HOME}/consul_setup_conplete/
touch ${HOME}/.consul_complete

exit $?
