#!/bin/bash


##############################################
SCRIPTS_DIR="${HOME}/bootstrap_local_repo_hapgmq_svr"
PRODUCT=$(cat /sys/class/dmi/id/sys_vendor)
OVIRT='.*oVirt.*'
HYPERV='Microsoft.*'
VMWARE='VMware.*'
USERNAME=$(whoami)
HOST_IPS=$(hostname -I)
MY_HOSTNAME=$(hostname | awk -F . '{print $1}')
##############################################
if [[ ${USERNAME} != "root" ]]; then
    echo "Please Run as Root"
    exit 0
fi


if [[ -f ${HOME}/bootstrap_local_repo_hapgmq_svr_complete/.bootstrap_complete ]]; then
    echo "############################################"
    echo ""
    echo " Bootstrap Has already be run "
    echo ""
    echo " if you wish to force it to rerun "
    echo " rm ${HOME}/bootstrap_local_repo_hapgmq_svr_complete/.bootstrap_complete "
    echo " and try again "
    echo ""
    echo "############################################"
    exit 0
fi

echo ""
echo "BootStrap Version 4.3"
echo ""
echo "You are Bootstraping your CentOS VM  BootStrap Version 4.3"
echo ""
echo "this may take a few mins"
echo ""

##############################################
#echo ""
#echo "unpack bootstrapreposvr"
#tar -xvf bootstrapreposvr.tar.xz

##############################################
echo "Fix Bashrc File"
mv -f ${SCRIPTS_DIR}/bashrc ${HOME}/.bashrc
##############################################
# make sure everything has the right ownership/group
chown -R ${USERNAME}:${USERNAME} ${HOME}/
# use the new bashrc
source ${HOME}/.bashrc
##############################################

echo "USE THE LOCAL REPO"
       rm -f /etc/yum.repos.d/{epel.repo,epel-testing.repo,CentOS-*.repo}
echo "Hello, what is the IP Address of the local repo server?"
      read VARIP

       sed -i s/LOCAL_REPO_IP/"${VARIP}/" ${SCRIPTS_DIR}/local.repo
       rm -f /etc/yum.repos.d/local.repo
       mv ${SCRIPTS_DIR}/local.repo /etc/yum.repos.d/
       mkdir ${HOME}/.pip/
       cat > ${HOME}/.pip/pip.conf << EOF

[global]
index-url = http://${VARIP}/pip/simple

EOF

echo ""
echo "Update System"
yum -y update

##############################################
echo "Install Key Tools"
chown -R root:root /root/
yum -y install vim net-tools bash-completion policycoreutils-python expect cockpit cockpit-ws cockpit-storaged cockpit-packagekit cockpit-pcp cockpit-dashboard unzip wget gcc make python3-devel epel-release firewalld mlocate yum-utils screen rsync yamllint pcp
##############################################

echo "Install Rest of Tools"
yum install -y python3-pip jq python-demjson
echo "Add bash-completion"
pip3 completion --bash >> /etc/bash_completion.d/pip
##############################################
echo ""
echo ""
echo "Enable Firewalld"
systemctl unmask firewalld
systemctl enable firewalld
systemctl start  firewalld
systemctl status  firewalld
##############################################
echo ""
echo ""
echo "Enable Cockpit"
systemctl enable cockpit.socket
systemctl start cockpit.socket
systemctl status cockpit.socket
##############################################
echo ""
echo ""
echo "VM Type Daemons"
if [[ ${PRODUCT} =~ ${OVIRT} ]] ; then
        yum -y install open-vm-tools
        echo "Enable oVirt Tools"
        systemctl enable ovirt-guest-agent.service
        systemctl start ovirt-guest-agent.service
        systemctl status ovirt-guest-agent.service

elif [[ ${PRODUCT} =~ ${HYPERV} ]] ; then
        echo "Optimize for Hyper-V"
        echo 'SUBSYSTEM=="memory", ACTION=="add", ATTR{state}="online"' > /etc/udev/rules.d/100-balloon.rules

elif [[ ${PRODUCT} =~ ${VMWARE} ]] ; then
        yum -y install open-vm-tools
        echo "Enable VMware Tools"
        systemctl enable vmtoolsd.service
        systemctl start vmtoolsd.service
        systemctl status vmtoolsd.service
fi
##############################################
echo ""
echo "Firewall"
firewall-cmd --permanent --add-port=22/tcp
firewall-cmd --permanent --add-port=9090/tcp
firewall-cmd --reload



echo ""
echo "Add Session Logging"
mv ${SCRIPTS_DIR}/bash.bash_logout /etc/
mv ${SCRIPTS_DIR}/bash_login.sh /etc/profile.d/

###########################
echo "Clean up"
mkdir ${HOME}/bootstrap_local_repo_hapgmq_svr_complete
touch ${HOME}/bootstrap_local_repo_hapgmq_svr_complete/.bootstrap_complete
mv ${HOME}/bootstrap_local_repo_hapgmq_svr_complete ${HOME}/bootstrap_local_repo_hapgmq_svr_complete/
mv ${HOME}/bootstrap_local_repo_hapgmq_svr_complete.tar.xz ${HOME}/bootstrap_local_repo_hapgmq_svr_complete/



echo ""
echo "****************************"
echo "    REBOOT! in 10 seconds"
echo "****************************"
echo ""
sleep 10
shutdown -r now
